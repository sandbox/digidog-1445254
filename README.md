# CLE - Contextual Links Everywhere

CLE is a lightweight UI tweaking module to extend the use of the new contextual links by moving common action tabs into the contextual links tooltip. NOTE: This was a sandbox project under Drupal 7 and many things have changed to the better in Drupal 8 and above.

### Why and Why not ...

By default, view mode full node pages or user pages have no contextual links enabled in Drupal core. While for some of you it may feel inconsequent, this is reasonable based on the history of why contextual links were introduced and why they are not everywhere: The main reason to introduce them was to have local tasks available for each node on list views. And on view mode full pages there are tabs to manage primary and secondary local tasks to grant accessibility in a general manner.

Depending on use-cases it still may feel cluttering and the tabs should be moved to the contextual links on every type of view including full view modes of content. This is where CLE comes in.

Warning: Usability initiative vs. accessibility initiative. Like so often: The functionality of CLE is an accessibility issue for many reasons. That's why this is moved to a contributed optional sandbox project. It makes sense to use it for portals with a known group of users, where accessibility issues are utterly out of question and keeping the interface clean has priority. Moving all local tasks from tabs to contextual links may break accessibility rules for non-mouse users and browsers with JS disabled or text based browsers. (We work on this by keeping tabs active but hidden via CSS).

### What CLE is doing

The CLE module renders local tasks from the primary and secondary local tabs to contextual links and enables you to have contextual links not only on node lists and blocks, but also on view mode full node pages, node forms, user pages and everywhere else, expandable to all areas we can think of, depending on how much help I get here for it. Additionally, the primary and secondary tabs can be disabled for this regions in two ways:

- hidden via CSS to keep accessibility for text-based or keyboard-tab-based browsing
- programmatically deactivated completely.

### Recommendation

Use CLE together with the [CCL](http://drupal.org/project/ccl) module to have full control over contextual links and available tasks in them. That's why I choose the near name. Feel free to discuss a merge in the [issue queue](http://drupal.org/project/issues/1445254). CCL enables you to add custom contextual links to the contextual links menu.

@TODO Future plan is to have another option to decide which user roles and which paths/\*/area this option should be active, what makes sense for projects, where accessibility issues have priority in one area and a clean interface has priority in another area. Feel free to post a feature request issue to ask for another area where you think the contextual links option should be added.

Feel free to post an [issue](http://drupal.org/project/issues/1445254 "CLE issues") to ask for the future of this project. (Core/Contrib/Merge)

## Issues which have inspired the need of CLE:

1. [http://drupal.org/node/1019486](http://drupal.org/node/1019486) Views module: Display fields in a contextual links block
2. [http://drupal.org/node/951088](http://drupal.org/node/951088) Contextual links instead of tabs
3. [Contextual links backport](http://drupal.org/project/contextual "Contextual links backport")
4. [working with contextual links](http://drupal.org/documentation/modules/contextual "working with contextual links")
5. [contextual-links-example-object.tpl.php](http://api.drupal.org/api/examples/contextual_links_example!contextual-links-example-object.tpl.php/7 "contextual-links-example-object.tpl.php")
6. [Drupal 7 tip: Add contextual links to anything](http://dominiquedecooman.com/blog/drupal-7-tip-add-contextual-links-anything " Add contextual links to anything")
7. [core issues for contextual links](http://drupal.org/project/issues/drupal?text=contextual+links "core issues for contextual links")
8. [No contextual links on full node view is confusing](http://drupal.org/node/842328 "No contextual links on full node view is confusing") - A favourite issue of mine and a discussion I follow and where I agress with, that it is confusing indeed
9. [And here an interesting core issue about the history of contextual links: "D7UX: Put edit links on everything"](http://drupal.org/node/473268 " Put edit links on everything")
10. [https://github.com/swentel/contextual/blob/master/contextual.module](https://github.com/swentel/contextual/blob/master/contextual.module) swentels D6 adaption of the contextual links idea

### Supporting organizations: 
[MAROQQO studios](https://www.drupal.org/maroqqo-studios)
has supported the project by financing it and allowing working on it in company time.

### Project information

- Minimally maintained  
    Maintainers monitor issues, but fast responses are not guaranteed.
- Maintenance fixes only  
    Considered feature-complete by its maintainers.
- Module categories: [Administration Tools](https://www.drupal.org/project/project_module?f%5B2%5D=im_vid_3%3A53)
- Created by [dqd](https://www.drupal.org/u/dqd) on 18 February 2012, updated 31 August 2024