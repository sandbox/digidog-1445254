<?php

/**
 * @file
 *
 * @author Digidog
 */

/**
 * Settings form for CLE.
 */
function cle_settings() {

    $functions = array (
        'name'          => t('contextual links everywhere'),
        'enable'        => t('enable/disable contextual links'),
        'notabs'        => t('show/hide tabs via CSS (not supported yet)')
        );

    $targets = array (
        'full_node'      => t('view mode full node pages'),
        'edit_node'      => t('node edit form pages'),
        'full_user'      => t('view mode full user pages'),
        'edit_user'      => t('user edit form pages')
        );

    $form = array();

    foreach ($targets as $key => $value) {
        $form[$key] = array(
            '#type'         => 'fieldset',
            '#collapsible'  => TRUE,
            '#collapsed'    => FALSE,
            '#title'        => ucfirst($functions['name']).t(' on ').$value,
            '#weight'       => '-21',
            '#description'  => t('<p><em>Render primary and secondary tasks into '
                    .$functions['name'].$value.
                    '</em></p>'
                    )
        );
            $form[$key]['on_off'] = array(
            '#type'          => 'checkbox',
            '#title'         => ucfirst($functions['enable']).t(' on ').$value,
            '#default_value' => variable_get('on_off', 0),
            '#description'   => t('Enable/disable rendering local tasks into '
                    .$functions['name'].$value.
                    '. This will NOT remove the primary and secondary tabs.'
                     )
            );
            $form[$key]['notabs'] = array(
            '#type'          => 'checkbox',
            '#title'         => ucfirst($functions['notabs']).t(' on ').$value,
            '#default_value' => variable_get('notabs', 0),
            '#description'   => t(
                    $functions['notabs'].' '.$value.'
                    This does not break accessibility,
                    since navigating by keyboard
                    tabbing and access with text based browsers
                    is still possible.'
                 )
            );
        }

  return system_settings_form($form);
}
